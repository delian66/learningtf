#!/usr/bin/python
import m
(train_images, train_labels), (test_images, test_labels) = m.tf.keras.datasets.mnist.load_data()
train_images = train_images/255.0
test_images = test_images/255.0
###############################################################################################
# Create a basic model instance
model = m.create_model()
# Create checkpoint callback
cp_callback = m.tf.keras.callbacks.ModelCheckpoint(m.checkpoint_path, save_weights_only=True, verbose=1)
model.fit(train_images, train_labels,  epochs = 5, validation_data = (test_images,test_labels), callbacks = [cp_callback])  # pass callback to training
loss, acc = model.evaluate(test_images, test_labels)
print("Trained model, accuracy: {:5.2f}%".format(100*acc))
model.summary()
