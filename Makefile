
help: ## Show this help: a short list of most usefull make targets, with their descriptions.
	@grep -P '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

hello: ## Test that Tensorflow is properly installed and print its version
	python hello.py

train_mnist: ## Train mnist model, and save it.
	python load_save/crunch_and_save.py

load_mnist: ## Load the saved mnist model and use it to recognize stuff.
	python load_save/load_and_test.py

