#!/usr/bin/python
import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))
print("Tensorflow version: {0}".format(tf.__version__))
