This repo contains examples derived from the TensorFlow documentation at:

https://www.tensorflow.org/tutorials/
and
https://www.tensorflow.org/tutorials/keras/save_and_restore_models

0. make:
![Make.jpg](https://gitlab.com/delian66/learningtf/raw/master/screenshots/make.jpg "make.jpg")
	
1. hello.py :

![Hello.jpg](https://gitlab.com/delian66/learningtf/raw/master/screenshots/hello.jpg "hello.jpg")

2. load_save/crunch_and_save.py :

![alt text](https://gitlab.com/delian66/learningtf/raw/master/screenshots/crunch_and_save.jpg "crunch_and_save.jpg")

3. load_save/load_and_test.py :

![alt text](https://gitlab.com/delian66/learningtf/raw/master/screenshots/load_and_test.jpg "load_and_test.jpg")

